// 1  Print the number of integers in an array that are above the given input and the number that are below, e.g. for the array [1, 5, 2, 1, 10] with input 6, print “above: 1, below: 4”

let countAboveBelow = (arr, input) => {
   let above = 0;
   let below = 0;
   arr.forEach(el=> el > input? above++ : below++ )
    console.log(`above: ${above}, below: ${below}`)
}

// 2. Rotate the characters in a string by a given input and have the overflow appear at the beginning, e.g. “MyString” rotated by 2 is “ngMyStri”.

let rotateString = (str, digits) => {
   if(!str || str.length === 0) return str
   let arr = str.split("")
   for(let i=0; i<digits; i++){
        arr.unshift(arr.pop())
    }
    return arr.join("")   
}

// 3.  If you could change 1 thing about your favorite framework/language/platform (pick one), what would it be?

`I wish Nodejs was also built for cpu intensive tasks and multithreading was straight forward to implement. Although, its not the right pick for cpu intensive applications
and other framework/languages are definately better fit. I do like Nodejs its easy to spin up and use but i still wouldn't call it my favorite framework. Over the years my 
mindset have changed to using the right tool for the right problem and associating myself less with the love of frameworks. I like to focus on high level principles, patterns, architecture
and concepts rather than frameworks/languages.`
